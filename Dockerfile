FROM python:3.8.10

ENV PATH="/scripts:${PATH}"
COPY ./requirements.txt /requirements.txt

RUN apt-get update && apt-get install -y python3-dev default-libmysqlclient-dev --no-install-recommends
RUN pip install -r /requirements.txt

RUN mkdir /app
COPY ./app /app
WORKDIR /app
COPY ./scripts /scripts

RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static

RUN adduser --disabled-password user
RUN chown -R user:user /vol
RUN chmod -R 755 /vol/web
USER user

CMD ["entrypoint.sh"]