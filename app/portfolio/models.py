from django.db import models

# Create your models here.


class PortfolioItem(models.Model):
    name = models.CharField(max_length=100)
    img = models.ImageField(upload_to='pics')
    desc = models.TextField()

class FilesAdmin(models.Model):
    adminupload = models.FileField(upload_to='downloads')
    title = models.CharField(max_length=50)

    def __str__(self) -> str:
        return self.title