from django.shortcuts import redirect, render
from django.http import HttpResponse, response
from .models import FilesAdmin, PortfolioItem
from django.conf import settings
from django.core.mail import send_mail
from decouple import Csv,config

# Create your views here.

def index(request):
    portfolio_items = PortfolioItem.objects.all()
    files_admin = FilesAdmin.objects.all()
    return render(request,'index.html', {'portfolio_items': portfolio_items, 'files_admin':files_admin})

def contact(request):
    if request.method == "POST":
        sender_name = request.POST["name"]
        sender_email = request.POST["email"]
        sender_message = request.POST["comment"]

        subject = sender_name + ',Email: '+ sender_email +', is emailing you through your website!'
        message = sender_message
        email_from = settings.EMAIL_HOST_USER
        recipient_list = {config('EMAIL_RECIPIENT'),}
        send_mail(subject,message,email_from,recipient_list)
    return render(request,'contact.html')
    

def brawlProject(request):
    return render(request,'BrawlProject.html')

def portfolioItem(request, id):
    portfolio_item = PortfolioItem.objects.get(pk=id)
    return render(request,'portfolio-item.html', {'portfolio_item': portfolio_item})

def download(request,path):
    #file_path=os.path.join(settings.MEDIA_ROOT,path)
    file_path = path
    print(file_path)
    if os.path.exists(file_path):
        with open(file_path,'rb') as fh:
            response=HttpResponse(fh.read(),content_type="application/adminupload")
            response['Content-Disposition']='inline;filename'+os.path.basename(file_path)
            return response

    raise Http404;

