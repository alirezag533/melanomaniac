# Generated by Django 3.2 on 2021-04-26 16:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='portfolioitem',
            name='desc',
            field=models.TextField(default='empty'),
            preserve_default=False,
        ),
    ]
